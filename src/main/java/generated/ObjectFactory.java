
package generated;

import javax.xml.bind.annotation.XmlRegistry;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the generated package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {


    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: generated
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link NewItemsDTORequest }
     * 
     */
    public NewItemsDTORequest createNewItemsDTORequest() {
        return new NewItemsDTORequest();
    }

    /**
     * Create an instance of {@link Items }
     * 
     */
    public Items createItems() {
        return new Items();
    }

    /**
     * Create an instance of {@link NewItemsDTOResponse }
     * 
     */
    public NewItemsDTOResponse createNewItemsDTOResponse() {
        return new NewItemsDTOResponse();
    }

    /**
     * Create an instance of {@link GetItemsDetailsRequest }
     * 
     */
    public GetItemsDetailsRequest createGetItemsDetailsRequest() {
        return new GetItemsDetailsRequest();
    }

    /**
     * Create an instance of {@link UpdateItemsResponse }
     * 
     */
    public UpdateItemsResponse createUpdateItemsResponse() {
        return new UpdateItemsResponse();
    }

    /**
     * Create an instance of {@link GetAllItemsResponse }
     * 
     */
    public GetAllItemsResponse createGetAllItemsResponse() {
        return new GetAllItemsResponse();
    }

    /**
     * Create an instance of {@link DeleteItemsResponse }
     * 
     */
    public DeleteItemsResponse createDeleteItemsResponse() {
        return new DeleteItemsResponse();
    }

    /**
     * Create an instance of {@link GetItemsDetailsResponse }
     * 
     */
    public GetItemsDetailsResponse createGetItemsDetailsResponse() {
        return new GetItemsDetailsResponse();
    }

    /**
     * Create an instance of {@link UpdateItemsRequest }
     * 
     */
    public UpdateItemsRequest createUpdateItemsRequest() {
        return new UpdateItemsRequest();
    }

    /**
     * Create an instance of {@link DeleteItemsRequest }
     * 
     */
    public DeleteItemsRequest createDeleteItemsRequest() {
        return new DeleteItemsRequest();
    }

    /**
     * Create an instance of {@link GetAllItemsRequest }
     * 
     */
    public GetAllItemsRequest createGetAllItemsRequest() {
        return new GetAllItemsRequest();
    }

}
