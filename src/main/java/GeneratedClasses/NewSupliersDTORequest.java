
package GeneratedClasses;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="Supliers" type="{https://rca.ac.rw/Yassin/soap-app}Supliers"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "supliers"
})
@XmlRootElement(name = "NewSupliersDTORequest", namespace = "https://rca.ac.rw/Yassin/soap-app")
public class NewSupliersDTORequest {

    @XmlElement(name = "Supliers", namespace = "https://rca.ac.rw/Yassin/soap-app", required = true)
    protected Supliers supliers;

    /**
     * Gets the value of the supliers property.
     * 
     * @return
     *     possible object is
     *     {@link Supliers }
     *     
     */
    public Supliers getSupliers() {
        return supliers;
    }

    /**
     * Sets the value of the supliers property.
     * 
     * @param value
     *     allowed object is
     *     {@link Supliers }
     *     
     */
    public void setSupliers(Supliers value) {
        this.supliers = value;
    }

}
