
package GeneratedClasses;

import javax.xml.bind.annotation.XmlRegistry;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the GeneratedClasses package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {


    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: GeneratedClasses
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link GetAllSupliersRequest }
     * 
     */
    public GetAllSupliersRequest createGetAllSupliersRequest() {
        return new GetAllSupliersRequest();
    }

    /**
     * Create an instance of {@link GetSupliersDetailsRequest }
     * 
     */
    public GetSupliersDetailsRequest createGetSupliersDetailsRequest() {
        return new GetSupliersDetailsRequest();
    }

    /**
     * Create an instance of {@link DeleteSuplierRequest }
     * 
     */
    public DeleteSuplierRequest createDeleteSuplierRequest() {
        return new DeleteSuplierRequest();
    }

    /**
     * Create an instance of {@link GetSuplierDetailsResponse }
     * 
     */
    public GetSuplierDetailsResponse createGetSuplierDetailsResponse() {
        return new GetSuplierDetailsResponse();
    }

    /**
     * Create an instance of {@link Supliers }
     * 
     */
    public Supliers createSupliers() {
        return new Supliers();
    }

    /**
     * Create an instance of {@link NewSuplierDTOResponse }
     * 
     */
    public NewSuplierDTOResponse createNewSuplierDTOResponse() {
        return new NewSuplierDTOResponse();
    }

    /**
     * Create an instance of {@link GetAllSupliersResponse }
     * 
     */
    public GetAllSupliersResponse createGetAllSupliersResponse() {
        return new GetAllSupliersResponse();
    }

    /**
     * Create an instance of {@link NewSupliersDTORequest }
     * 
     */
    public NewSupliersDTORequest createNewSupliersDTORequest() {
        return new NewSupliersDTORequest();
    }

    /**
     * Create an instance of {@link UpdateSupplierRequest }
     * 
     */
    public UpdateSupplierRequest createUpdateSupplierRequest() {
        return new UpdateSupplierRequest();
    }

    /**
     * Create an instance of {@link UpdateSupplierResponse }
     * 
     */
    public UpdateSupplierResponse createUpdateSupplierResponse() {
        return new UpdateSupplierResponse();
    }

    /**
     * Create an instance of {@link DeleteSupliersResponse }
     * 
     */
    public DeleteSupliersResponse createDeleteSupliersResponse() {
        return new DeleteSupliersResponse();
    }

}
