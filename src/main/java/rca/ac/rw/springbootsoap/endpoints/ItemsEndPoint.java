package rca.ac.rw.springbootsoap.endpoints;

import GeneratedClasses.*;
import generated.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.ws.server.endpoint.annotation.PayloadRoot;
import org.springframework.ws.server.endpoint.annotation.RequestPayload;
import org.springframework.ws.server.endpoint.annotation.ResponsePayload;
import rca.ac.rw.springbootsoap.models.Items;
import rca.ac.rw.springbootsoap.repositories.IItemRepository;

public class ItemsEndPoint {
  private IItemRepository itemRepository;

  @Autowired
  public ItemsEndPoint(IItemRepository repository) {

    this.itemRepository = repository;
  }

  @PayloadRoot(namespace = "https://rca.ac.rw/Yassin/soap-app", localPart = "NewItemsDTORequest")
  @ResponsePayload
  public NewItemsDTOResponse create(@RequestPayload NewItemsDTORequest dto) {
    generated.Items __student = dto.getItems();

    Items _student = new Items(__student.getId(),__student.getPrice(),__student.getName(), __student.getCode(), __student.getStatus());

    Items student = itemRepository.save(_student);

    NewItemsDTOResponse studentDTO = new NewItemsDTOResponse();

    __student.setId(student.getId());

    studentDTO.setItems(__student);

    return studentDTO;
  }


  @PayloadRoot(namespace = "https://rca.ac.rw/Yassin/soap-app", localPart = "DeleteItemsRequest")
  @ResponsePayload
  public DeleteItemsResponse delete(@RequestPayload DeleteItemsRequest request){
    itemRepository.deleteById(request.getId());
    DeleteItemsResponse response = new DeleteItemsResponse();
    response.setMessage("Successfully deleted a message");
    return response;
  }

  @PayloadRoot(namespace = "https://rca.ac.rw/Yassin/soap-app", localPart = "UpdateItemsRequest")
  @ResponsePayload
  public UpdateItemsResponse update(@RequestPayload UpdateItemsRequest request){
    generated.Items __student = request.getItems();

   Items _student = new Items(__student.getId(),__student.getPrice(),__student.getName(), __student.getCode(), __student.getStatus());
    _student.setId(__student.getId());

    Items student = itemRepository.save(_student);

    UpdateItemsResponse studentDTO = new UpdateItemsResponse();

    __student.setId(student.getId());

    studentDTO.setItems(__student);

    return studentDTO;
  }


}
