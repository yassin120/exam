package rca.ac.rw.springbootsoap.endpoints;

import GeneratedClasses.*;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.ws.server.endpoint.annotation.Endpoint;
import org.springframework.ws.server.endpoint.annotation.PayloadRoot;
import org.springframework.ws.server.endpoint.annotation.RequestPayload;
import org.springframework.ws.server.endpoint.annotation.ResponsePayload;
import rca.ac.rw.springbootsoap.repositories.ISupliersRepository;
import rca.ac.rw.springbootsoap.models.Supliers;

import java.util.List;
import java.util.Optional;

@Endpoint
public class SupliersEndPoint {
    private ISupliersRepository supplierRepository;

    @Autowired
    public SupliersEndPoint(ISupliersRepository repository) {

        this.supplierRepository = repository;
    }

    @PayloadRoot(namespace = "https://rca.ac.rw/Yassin/soap-app", localPart = "NewSupliersDTORequest")
    @ResponsePayload
    public NewSuplierDTOResponse create(@RequestPayload NewSupliersDTORequest dto) {
        GeneratedClasses.Supliers __student = dto.getSupliers();

        Supliers _student = new Supliers(__student.getId(),__student.getName(), __student.getEmail(), __student.getMobile());

        Supliers student = supplierRepository.save(_student);

       NewSuplierDTOResponse studentDTO = new NewSuplierDTOResponse();

        __student.setId(student.getId());

        studentDTO.setSupliers(__student);

        return studentDTO;
    }

    @PayloadRoot(namespace = "https://rca.ac.rw/Yassin/soap-app", localPart = "GetAllSupliersRequest")
    @ResponsePayload
    public GetAllSupliersResponse findAll(@RequestPayload GetAllSupliersRequest request){

        List<Supliers> students = supplierRepository.findAll();

        GetAllSupliersResponse response = new GetAllSupliersResponse();

        for (Supliers student: students){
            GeneratedClasses.Supliers _student = new GeneratedClasses.Supliers();
            _student.setId(student.getId());
            _student.setName(student.getName());
            _student.setName(student.getEmail());
            _student.setMobile(student.getMobile());
            response.getSupliers().add(_student);
        }

        return response;
    }

    @PayloadRoot(namespace = "https://rca.ac.rw/Yassin/soap-app", localPart = "GetSupliersDetailsRequest")
    @ResponsePayload
    public GetSuplierDetailsResponse findById(@RequestPayload GetSupliersDetailsRequest request){
        Optional<Supliers> _student = supplierRepository.findById(request.getId());

        if(!_student.isPresent())
            return new GetSuplierDetailsResponse();

        Supliers student = _student.get();

        GetSuplierDetailsResponse response = new GetSuplierDetailsResponse();

        GeneratedClasses.Supliers __student = new GeneratedClasses.Supliers();
        __student.setId(student.getId());
        __student.setName(student.getName());
        __student.setEmail(student.getEmail());
        __student.setMobile(student.getMobile());
        response.setStudent(__student);

        return response;
    }

    @PayloadRoot(namespace = "https://rca.ac.rw/Yassin/soap-app", localPart = "DeleteSuplierRequest")
    @ResponsePayload
    public DeleteSupliersResponse delete(@RequestPayload DeleteSuplierRequest request){
        supplierRepository.deleteById(request.getId());
        DeleteSupliersResponse response = new DeleteSupliersResponse();
        response.setMessage("Successfully deleted a message");
        return response;
    }

    @PayloadRoot(namespace = "https://rca.ac.rw/Yassin/soap-app", localPart = "UpdateSupplierRequest")
    @ResponsePayload
    public UpdateSupplierResponse update(@RequestPayload UpdateSupplierRequest request){
        GeneratedClasses.Supliers __student = request.getStudent();

        Supliers _student = new Supliers(__student.getId(),__student.getName(), __student.getEmail(), __student.getMobile());
        _student.setId(__student.getId());

        Supliers student = supplierRepository.save(_student);

        UpdateSupplierResponse studentDTO = new UpdateSupplierResponse();

        __student.setId(student.getId());

        studentDTO.setSupliers(__student);

        return studentDTO;
    }


}