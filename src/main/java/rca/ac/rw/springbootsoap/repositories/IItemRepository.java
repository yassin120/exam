package rca.ac.rw.springbootsoap.repositories;


import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import rca.ac.rw.springbootsoap.models.Supliers;

@Repository
public interface IItemRepository extends JpaRepository<Supliers,Long> {
}
