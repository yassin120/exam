package rca.ac.rw.springbootsoap.models;
import javax.persistence.*;

@Entity
@Table(name = "suppliers")
public class Supliers {

  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  public long id;
  public String Name;
  public String Email;
  public String mobile;

  public Supliers() {
  }

  public Supliers(long id, String name, String email, String mobile) {
    this.id = id;
    this.Name = name;
    this.Email = email;
    this.mobile = mobile;
  }

  public long getId() {
    return id;
  }

  public void setId(long id) {
    this.id = id;
  }

  public String getName() {
    return Name;
  }

  public void setName(String name) {
    Name = name;
  }

  public String getEmail() {
    return Email;
  }

  public void setEmail(String email) {
    Email = email;
  }

  public String getMobile() {
    return mobile;
  }

  public void setMobile(String mobile) {
    this.mobile = mobile;
  }
}