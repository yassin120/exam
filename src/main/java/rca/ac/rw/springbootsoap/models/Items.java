package rca.ac.rw.springbootsoap.models;


import javax.persistence.*;

@Entity
@Table(name = "Items")
public class Items {
  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  public long id;
  public  String code;
  public  String name;
  public  String price;

  public String status;


  public Items(long id, String code, String name, String price, String status) {
    this.id = id;
    this.code = code;
    this.name = name;
    this.price = price;
    this.status = status;
  }

  public Items() {

  }

  public long getId() {
    return id;
  }

  public void setId(long id) {
    this.id = id;
  }

  public String getCode() {
    return code;
  }

  public void setCode(String code) {
    this.code = code;
  }

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public String getPrice() {
    return price;
  }

  public void setPrice(String price) {
    this.price = price;
  }

  public String getStatus() {
    return status;
  }

  public void setStatus(String status) {
    this.status = status;
  }

}
