package rca.ac.rw.springbootsoap.config;

import org.springframework.boot.web.servlet.ServletRegistrationBean;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.io.ClassPathResource;
import org.springframework.ws.config.annotation.EnableWs;
import org.springframework.ws.transport.http.MessageDispatcherServlet;
import org.springframework.ws.wsdl.wsdl11.DefaultWsdl11Definition;
import org.springframework.xml.xsd.SimpleXsdSchema;
import org.springframework.xml.xsd.XsdSchema;


@EnableWs
@Configuration
public class WebServiceConfig {

    @Bean
    public ServletRegistrationBean<MessageDispatcherServlet> messageDispatcherServlet(ApplicationContext context) {
        MessageDispatcherServlet messageDispatcherServlet = new MessageDispatcherServlet();
        messageDispatcherServlet.setApplicationContext(context);
        messageDispatcherServlet.setTransformWsdlLocations(true);
        return new ServletRegistrationBean<MessageDispatcherServlet>(messageDispatcherServlet, "/ws/Yassin/*");
    }

    // /ws/yassin/supliers.wsdl
    @Bean(name = "supliers")
    public DefaultWsdl11Definition supplierWsdl(XsdSchema supplierSchema) {
        DefaultWsdl11Definition definition = new DefaultWsdl11Definition();
        definition.setPortTypeName("SupliersPort");
        definition.setTargetNamespace("https://rca.ac.rw/Yassin/soap-app");
        definition.setLocationUri("/ws/Yassin");
        definition.setSchema(supplierSchema);
        return definition;
    }

    // /ws/yassin/Items.wsdl
    @Bean(name = "supliers")
    public DefaultWsdl11Definition ItemWsdl(XsdSchema itemsSchema) {
        DefaultWsdl11Definition definition = new DefaultWsdl11Definition();
        definition.setPortTypeName("ItemsPort");
        definition.setTargetNamespace("https://rca.ac.rw/Yassin/soap-app");
        definition.setLocationUri("/ws/Yassin");
        definition.setSchema(itemsSchema);
        return definition;
    }

    @Bean
    public XsdSchema supplierSchema() {
        return new SimpleXsdSchema(new ClassPathResource("app.xsd"));
    }

    @Bean
    public XsdSchema itemsSchema() {
        return new SimpleXsdSchema(new ClassPathResource("item.xsd"));
    }





}